import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;


import net.sourceforge.tess4j.*;
import net.sourceforge.tess4j.ITessAPI.TessPageIteratorLevel;

public class TesseractExample {

	public static void main(String[] args) {
		
		//Test OCR
		File image = new File("Resources/Sample_Spanish.jpg");
		ITesseract instance = new Tesseract();
		try{
			String result = instance.doOCR(image);	
			System.out.println(result);
		}catch(TesseractException ex){
			System.out.println(ex.toString());
		}
		
		//Test Words
		int pageIteratorLevel = TessPageIteratorLevel.RIL_WORD;
		BufferedImage bi = null;
		try {
			bi = ImageIO.read(image);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Word> result = instance.getWords(bi, pageIteratorLevel);
		System.out.println("Words");
		for(Word w : result){
			System.out.println(w);
		}
		
		//Test SegmentedRegions
		try {
			int level = TessPageIteratorLevel.RIL_SYMBOL;
			List<Rectangle> res = instance.getSegmentedRegions(bi, level);
			System.out.println("\nSegmentedRegions\n");
			System.out.println("N1 of regions: "+res.size());
			for(Rectangle r : res){
				System.out.println(r);
			}
		} catch (TesseractException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
